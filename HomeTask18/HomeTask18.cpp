﻿// HomeTask18.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

template <class MyType>
class Stack {

private:
    MyType* bottom;
    MyType* top;
    int _size = 10;

public:
    int getSize()
    {
        return _size;
    }

    Stack(int initSize) 
    {
        _size = initSize;
        bottom = new MyType[_size];
        top = bottom;
    }

    ~Stack() 
    {
        delete[] bottom;
    }

    int getItemsNumber()
    {
        return top - bottom;
    }

    void resize(int newSize)
    {
        MyType* newArr = new MyType[newSize];

        for (int i = 0; i < getItemsNumber(); i++)
        {
            *(newArr + i) = *(bottom + i);
        }

        delete[] bottom;
        bottom = newArr;

        newArr = nullptr;

        top = bottom + _size;
        _size = newSize;

        std::cout << "array have been resized to size " << newSize << "\n";
    }

    bool isFull() 
    {
        return _size == getItemsNumber();

    }

    void push(MyType item)
    {
        if (isFull())
        {
            resize(_size * 2);
        }
        *top = item;
        top++;
    }

    MyType pop()
    {
        top--;
        return *top;
    }

    void printStack() 
    {
        for (int i = 0; i < getItemsNumber(); i++)
        {
            std::cout << *(bottom + i) << "\n";
        }
    }
};


int main()
{

    Stack<int> iStack(3);

    iStack.push(1);
    iStack.push(2);
    iStack.push(3);
    iStack.push(4);
    iStack.printStack();
    iStack.push(5);
    iStack.printStack();
    iStack.push(6);
    iStack.push(7);
    iStack.printStack();

    std::cout << "first pop: " << iStack.pop() << "\n";
    std::cout << "second pop: " << iStack.pop() << "\n";
    iStack.printStack();

    Stack<std::string> sStack(2);
    sStack.push(" World!");
    sStack.push(" beautiful");
    sStack.push(" my");
    sStack.push("Hello,");

    sStack.printStack();
    std::cout << sStack.pop();
    std::cout << sStack.pop();
    std::cout << sStack.pop();
    std::cout << sStack.pop();

    sStack.printStack();
}